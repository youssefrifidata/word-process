package com.emsi.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.emsi.entities.AppUser;
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	private AccountService accountService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser u = accountService.findUserByUserName(username);
		if (u == null) throw new UsernameNotFoundException(username);
		Collection<GrantedAuthority> autorities = new ArrayList<>();
		u.getRoles().forEach(r ->
		autorities.add(new SimpleGrantedAuthority(r.getRoleName())));
		return  new User(u.getUsername(),u.getPassword(),autorities);
		
	}

}

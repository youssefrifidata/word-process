package com.emsi.service;

import com.emsi.entities.AppRole;
import com.emsi.entities.AppUser;

public interface AccountService {
	public AppUser saveUser(AppUser user);
	public AppRole saveRole(AppRole role);
	public void addRoleToUser(String username, String Role);
	public AppUser findUserByUserName(String username);
}

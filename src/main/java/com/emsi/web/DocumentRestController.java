package com.emsi.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.emsi.dao.DocumentRepository;
import com.emsi.entities.Document;

@RestController
public class DocumentRestController {
	@Autowired
	private DocumentRepository documentRepository;
	@RequestMapping(value = "/documents", method = RequestMethod.GET)
	public List<Document> getDocuments() {
		return documentRepository.findAll();
	}

	
}

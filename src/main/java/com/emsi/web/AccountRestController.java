package com.emsi.web;

import java.util.List;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.emsi.dao.DocumentRepository;
import com.emsi.dao.UserRepository;
import com.emsi.entities.AppUser;
import com.emsi.entities.Document;
import com.emsi.service.AccountService;

@RestController
public class AccountRestController {
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private UserRepository userRepository;
	
	@PostMapping("/register")
	public AppUser register(@RequestBody RegisterForm userform) {
		if(!userform.getPassword().equals(userform.getRepassword())) {
			throw new RuntimeException("You Must confirm your password");
	   }
		AppUser user = accountService.findUserByUserName(userform.getUsername());
		if(user != null) throw new RuntimeException("this user already exists");
		AppUser appUser = new AppUser();
		appUser.setUsername(userform.getUsername());
		appUser.setPassword(userform.getPassword());
		accountService.saveUser(appUser);
		accountService.addRoleToUser(userform.getUsername(),userform.getRole());
		return appUser;
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public List<AppUser> getUsers() {
		return userRepository.findAll();
	}
}

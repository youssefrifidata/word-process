package com.emsi.security;

public class SecurityConstants {
	public static final String SECRET = "youssefPopia1993";
	public static final long EXPIRATION_TIME = 864_000_000;  // 10 Tage
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
}

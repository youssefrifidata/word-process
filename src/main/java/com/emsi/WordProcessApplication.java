package com.emsi;


import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.emsi.dao.DocumentRepository;
import com.emsi.entities.AppRole;
import com.emsi.entities.AppUser;
import com.emsi.entities.Document;
import com.emsi.service.AccountService;

@SpringBootApplication
public class WordProcessApplication implements CommandLineRunner {

	@Autowired
	private DocumentRepository documentRepository;
	@Autowired
	 private AccountService accountService;
	public static void main(String[] args) {
		
		SpringApplication.run(WordProcessApplication.class, args);
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	@Override
	public void run(String... args) throws Exception {
		Date d = new Date(System.currentTimeMillis());
		documentRepository.save(new Document(null, "word-1",d,"Mr RSE-1"));
		documentRepository.save(new Document(null, "word-2",d,"Mr RSE-2"));
		documentRepository.save(new Document(null, "word-3",d,"Mr RSE-3"));
		documentRepository.findAll().forEach(doc -> {
			System.out.println(doc.getName());
					});
		
		accountService.saveUser(new AppUser(null,"youssef","1234",null));
		accountService.saveUser(new AppUser(null,"popia","1234",null));
		accountService.saveRole(new AppRole(null,"ADMIN"));
		accountService.saveRole(new AppRole(null,"RSE"));

		accountService.addRoleToUser("youssef", "ADMIN");
		accountService.addRoleToUser("youssef", "RSE");
		accountService.addRoleToUser("popia", "RSE");
		
	}

}

package com.emsi.entities;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Document implements Serializable {
	private static final long serialVersionUID = -5118898161181356264L;
	@Id @GeneratedValue 
	private Long id;
	@NotNull
	private String name;
	@Temporal(TemporalType.TIME) private Date generationDate;
	private String responsible;
}

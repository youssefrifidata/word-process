package com.emsi.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.emsi.entities.Document;

public interface DocumentRepository extends JpaRepository<Document, Long> {

}

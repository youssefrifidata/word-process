package com.emsi.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.emsi.entities.AppRole;

public interface RoleRepository extends JpaRepository<AppRole, Long> {
	public AppRole findByRoleName(String roleName);
}

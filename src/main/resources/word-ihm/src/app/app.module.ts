import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DocumentComponent } from './business/document/document.component';
import { ListDocumentComponent } from './business/list-document/list-document.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { FormDocumentComponent } from './business/form-document/form-document.component';
import { HeaderComponent } from './header/header.component';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { DocumentService } from './services/document.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const appRoutes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'auth/signup', component: SignupComponent},
  {path: 'auth/signin', component: SigninComponent},
  {path: 'documents/new', component: FormDocumentComponent},
  {path: 'documents', component: ListDocumentComponent},
  {path: 'documents/view/:id', component: DocumentComponent},
  {path: '',redirectTo:'auth/signin',pathMatch:'full'}
]
@NgModule({
  declarations: [
    AppComponent,
    DocumentComponent,
    ListDocumentComponent,
    SignupComponent,
    SigninComponent,
    FormDocumentComponent,
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [AuthService, AuthGuardService, DocumentService],
  bootstrap: [AppComponent]
})
export class AppModule { }

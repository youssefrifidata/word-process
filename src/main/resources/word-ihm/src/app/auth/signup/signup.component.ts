import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  listUsers : any;

  ngOnInit(): void {
    this.getUsers();
  }
  
  getUsers(){
    this.authService.getUsers()
    .subscribe(resp=>{
      this.listUsers = resp;
   console.log(resp);
    },
  err=>{
    console.log(err);
  });
  }

  constructor(private authService: AuthService, private router : Router){
  }

  onRegister(userForm: any){
    this.authService.register(userForm)
    .subscribe(resp=>{
      console.log(resp);
      this.getUsers();

    },
  error=>{
    console.log(error);
  
  })
  }

}

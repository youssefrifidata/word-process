import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  ngOnInit(): void {

  }
  error: number = 0;
  constructor(private authService: AuthService, private router: Router) {
    // redirect to home if already logged in
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  onLogin(user: any) {
    this.authService.login(user)
      .subscribe(resp => {
        console.log(resp);
        let jwtToken = resp.headers.get('Authorization');
        console.log(jwtToken);
        this.router.navigateByUrl('/documents');
      },
        error => {
          console.log(error);
          this.error = 1;

        })
  }
}

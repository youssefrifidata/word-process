import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  private documentsUrl = '/api/documents/';  // URL to web api

  constructor(private http: HttpClient) { }

  getDocuments(): Observable<any> {
    // ajouter observe + comprendre le principe et corriger ! 
    return this.http.get(this.documentsUrl, { headers: new HttpHeaders({ 'Authorization': this.getToken() }) });
  }

  getToken() {
    return localStorage.getItem('token');
  }
}

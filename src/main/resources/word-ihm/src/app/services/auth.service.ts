import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as jwt_decode from "jwt-decode";
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loginUrl = '/api/login';
  private registerUrl = '/api/register';
  private usersUrl = '/api/users';

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(localStorage.getItem('currentUser'));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }


  getToken() {
    return localStorage.getItem('token');
  }

  login(user: any) {
    return this.http
      .post(this.loginUrl, user, { observe: 'response' }).pipe(map(resp => {
        console.log(resp);
        let jwtToken = resp.headers.get('Authorization');
        if (jwtToken) {
          this.saveToken(jwtToken);
          let decodedJwt = this.getDecodedJwtToken(jwtToken);
          localStorage.setItem('currentUser', decodedJwt.sub);
          console.log(decodedJwt)
          this.currentUserSubject.next(decodedJwt.sub);

        }

        return resp;
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    this.currentUserSubject.next(null);
  }


  register(userForm: any) {
    return this.http
      .post(this.registerUrl, userForm, { observe: 'response' });
  }

  getUsers(): Observable<any> {
    return this.http.get(this.usersUrl, { headers: new HttpHeaders({ 'Authorization': this.getToken() }) });
  }

  saveToken(jwt: string) {
    localStorage.setItem('token', jwt);
  }

  getDecodedJwtToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DocumentService } from 'src/app/services/document.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list-document',
  templateUrl: './list-document.component.html',
  styleUrls: ['./list-document.component.scss']
})
export class ListDocumentComponent implements OnInit {

  constructor(private documentsService : DocumentService){
 
  }

  listDocuments : any;

  ngOnInit(): void {
    this.getDocuments();
  }
  
  getDocuments(){
    this.documentsService.getDocuments()
    .subscribe(resp=>{
      this.listDocuments = resp;
   console.log(resp);
    },
  err=>{
    console.log(err);
  });
  }
  
}
